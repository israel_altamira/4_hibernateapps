package com.mkyong.stock.dao.impl;

import java.util.List;
 


import org.hibernate.classic.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
 


import com.mkyong.stock.dao.StockDao;
import com.mkyong.stock.model.Stock;
import com.mkyong.util.HibernateUtil;
 
public class StockDaoImpl implements StockDao {
 
	HibernateUtil sessionFactory;
	
	public void setSessionFactory(HibernateUtil hibernateUtil){
		sessionFactory = hibernateUtil;
	}
	
	public void save(Stock stock){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(stock);
		session.getTransaction().commit();
	}
 
	public void update(Stock stock){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.update(stock);
		session.getTransaction().commit();
	}
 
	public void delete(Stock stock){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(stock);
		session.getTransaction().commit();
	}
 
	public Stock findByStockCode(String stockCode){
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List products = session.createQuery("FROM Stock where stock_code = '" + stockCode + "'").list();
		session.getTransaction().commit();
		return (Stock)products.get(0);
	}
 
}