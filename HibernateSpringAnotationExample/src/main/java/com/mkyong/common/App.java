package com.mkyong.common;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.mkyong.stock.bo.StockBo;
import com.mkyong.stock.model.Stock;

public class App {
	public static void main(String[] args) {
		ApplicationContext appContext = new ClassPathXmlApplicationContext("spring/config/BeanLocations.xml");

		StockBo stockBo = (StockBo) appContext.getBean("stockBo");

		/** insert **/
		Stock stock = new Stock();
		stock.setStockCode("GBL-7680");
		stock.setStockName("Glb Avenger_2");
		stockBo.save(stock);

		/** select **/
		Stock stock2 = stockBo.findByStockCode("GBL-7680");
		System.out.println(stock2);

		/** update **/
		stock2.setStockName("Glb Avenger_3");
		stockBo.update(stock2);

		/** delete **/
		//stockBo.delete(stock2);

		System.out.println("Done");
	}
}