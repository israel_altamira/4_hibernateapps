package com.mkyong.common;

import org.hibernate.SessionFactory;
import org.hibernate.classic.Session;

import com.mkyong.stock.model.Stock;
import com.mkyong.util.HibernateUtil;

public class App {

    public static void main(String[] args) {

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

        /** insert **/
        Stock stock = new Stock();
        stock.setStockCode("GBL-7676");
        stock.setStockName("GlbMetalDCcomic__");
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save(stock);
        session.getTransaction().commit();

        stock.setStockName("Glb Metal DCcomic1");
        session.beginTransaction();
        session.update(stock);
        session.getTransaction().commit();

        System.out.println("Done");
    }
}